import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class AdvancedSearchPage {
    WebDriver driver;

    @FindBy(id = "all-words")
    private WebElement allWordsField;

    @FindBy(id = "exact-phrase")
    private WebElement exactPhraseField;

    @FindBy(id = "least-words")
    private WebElement leastWordsField;

    @FindBy(id = "without-words")
    private WebElement withoutWordsField;

    @FindBy(id = "title-is")
    private WebElement titleField;

    @FindBy(id = "author-is")
    private WebElement authorField;

    @FindBy(id = "facet-start-year")
    private WebElement startYearField;

    @FindBy(id = "facet-end-year")
    private WebElement endYearField;

    @FindBy(id = "submit-advanced-search")
    private WebElement searchButton;

    public AdvancedSearchPage(WebDriver driver) {
        this.driver = driver;
        PageFactory.initElements(driver, this);
    }

    public void search(String allWords, String exactPhrase, String leastWords, String withoutWords, String title, String author, String startYear, String endYear) {
        allWordsField.sendKeys(allWords);
        exactPhraseField.sendKeys(exactPhrase);
        leastWordsField.sendKeys(leastWords);
        withoutWordsField.sendKeys(withoutWords);
        titleField.sendKeys(title);
        authorField.sendKeys(author);
        startYearField.sendKeys(startYear);
        endYearField.sendKeys(endYear);
        searchButton.click();
    }
}