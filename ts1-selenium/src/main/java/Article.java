public class Article {
    private String title;
    private String doi;
    private String pubDate;

    public Article(String title, String doi, String pubDate) {
        this.title = title;
        this.doi = doi;
        this.pubDate = pubDate;
    }

    public String getTitle() {
        return title;
    }

    public String getDOI() {
        return doi;
    }

    public String getPubDate() {
        return pubDate;
    }
}