import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class ArticlePage {
    WebDriver driver;

    @FindBy(xpath = "//h1[@class='c-article-title']")
    WebElement articleTitle;

    @FindBy(xpath = "//p[abbr[contains(text(), 'DOI')]]/span[@class='c-bibliographic-information__value']")
    WebElement articleDOI;

    @FindBy(xpath = "//li[contains(text(), 'Published')]")
    WebElement articlePubDate;

    public ArticlePage(WebDriver driver) {
        this.driver = driver;
        PageFactory.initElements(driver, this);
    }

    public String getTitle() {
        return articleTitle.getText();
    }

    public String getDOI() {
        return articleDOI.getText();
    }

    public String getPubDate() {
        return articlePubDate.getText();
    }
}