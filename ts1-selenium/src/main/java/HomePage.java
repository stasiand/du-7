import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.time.Duration;

public class HomePage {
    WebDriver driver;

    @FindBy(linkText = "Log in")
    private WebElement loginLink;

    @FindBy(id = "homepage-search")
    private WebElement searchField;

    @FindBy(className = "app-homepage-hero__button")
    private WebElement searchButton;

    @FindBy(xpath = "//a[@href='/search?new-search=false']")
    private WebElement oldVersionLink;

    @FindBy(className = "open-search-options")
    private WebElement settingsButton;
    @FindBy(id = "advanced-search-link")
    private WebElement advancedSearchLink;

    @FindBy(xpath = "//button[@data-cc-action='accept']")
    private WebElement acceptCookiesButton;

    public HomePage(WebDriver driver) {
        this.driver = driver;
        PageFactory.initElements(driver, this);
    }

    public void navigateToLogin() {
        WebDriverWait wait = new WebDriverWait(driver, Duration.ofSeconds(5)); // ожидание до 10 секунд
        wait.until(ExpectedConditions.elementToBeClickable(loginLink));
        loginLink.click();
    }

    public void performSearch(String query) {
        searchField.sendKeys(query);
        searchButton.click();
    }

    public void navigateToOldVersion() {
        oldVersionLink.click();
    }
    public void openSettings() {
        settingsButton.click();
    }

    public void navigateToAdvancedSearch() {
        advancedSearchLink.click();
    }

    public void acceptCookies() {
        WebDriverWait wait = new WebDriverWait(driver, Duration.ofSeconds(5)); // ожидание до 10 секунд
        wait.until(ExpectedConditions.elementToBeClickable(acceptCookiesButton));
        acceptCookiesButton.click();
    }
}