import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class LoginPage {
    WebDriver driver;

    @FindBy(id = "login-email")
    private WebElement emailField;

    @FindBy(id = "email-submit")
    private WebElement emailSubmitButton;

    @FindBy(id = "login-password")
    private WebElement passwordField;

    @FindBy(id = "password-submit")
    private WebElement passwordSubmitButton;

    public LoginPage(WebDriver driver) {
        this.driver = driver;
        PageFactory.initElements(driver, this);
    }

    public void enterEmail(String email) {
        emailField.sendKeys(email);
        emailSubmitButton.click();
    }

    public void enterPassword(String password) {
        passwordField.sendKeys(password);
        passwordSubmitButton.click();
    }

    public void login(String email, String password) {
        enterEmail(email);
        enterPassword(password);
    }
}