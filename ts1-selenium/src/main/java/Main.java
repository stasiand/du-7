import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;

import java.util.ArrayList;
import java.util.List;

public class Main {
    public static void main(String[] args) {
        System.setProperty("webdriver.chrome.driver", "C:\\Users\\andre\\Downloads\\chromedriver-win64\\chromedriver-win64\\chromedriver.exe");
        ChromeOptions options = new ChromeOptions();
        options.addArguments("--remote-allow-origins=*");
        WebDriver driver = new ChromeDriver(options);
        driver.get("https://link.springer.com/");

        HomePage homePage = new HomePage(driver);

        homePage.acceptCookies();
        homePage.navigateToLogin();

        LoginPage loginPage = new LoginPage(driver);
        loginPage.login("andreystasiuk1488@gmail.com", "qwertytop1228");
        homePage.performSearch("ahoj");
        homePage.navigateToOldVersion();
        homePage.openSettings();
        homePage.navigateToAdvancedSearch();

        AdvancedSearchPage advancedSearchPage = new AdvancedSearchPage(driver);
        advancedSearchPage.search("Page Object Model", "", "Selenium Testing", "", "", "", "2024", "2024");

        SearchResultsPage searchResultsPage = new SearchResultsPage(driver);
        searchResultsPage.filterByArticle();
        int numberOfArticles = searchResultsPage.getNumberOfArticles();
        System.out.println("Number of articles: " + numberOfArticles);

        List<Article> articles = new ArrayList<>();
        for (int i = 0; i < 4 && i < numberOfArticles; i++) {
            searchResultsPage.openArticle(i);

            ArticlePage articlePage = new ArticlePage(driver);
            String title = articlePage.getTitle();
            String doi = articlePage.getDOI();
            String pubDate = articlePage.getPubDate();

            articles.add(new Article(title, doi, pubDate));

            System.out.println("Title: " + title);
            System.out.println("DOI: " + doi);
            System.out.println("Publication Date: " + pubDate);

            driver.navigate().back();
        }


        driver.quit();
    }
}