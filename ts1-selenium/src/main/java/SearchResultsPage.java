import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;


import java.util.List;

public class SearchResultsPage {
    WebDriver driver;

    @FindBy(xpath = "//a[@class='title']")
    List<WebElement> articleLinks;

    @FindBy(xpath = "//a[contains(@href, 'facet-content-type=%22Article%22')]")
    WebElement articleFilterLink;

    public SearchResultsPage(WebDriver driver) {
        this.driver = driver;
        PageFactory.initElements(driver, this);
    }

    public void openArticle(int index) {
        articleLinks.get(index).click();
    }

    public int getNumberOfArticles() {
        return articleLinks.size();
    }
    public void filterByArticle() {
        articleFilterLink.click();
    }
}