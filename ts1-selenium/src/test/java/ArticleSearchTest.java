import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;

import java.util.ArrayList;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

public class ArticleSearchTest {
    private WebDriver driver;
    private HomePage homePage;
    private LoginPage loginPage;
    private SearchResultsPage searchResultsPage;
    private ArticlePage articlePage;
    private List<Article> articles; // список для хранения статей

    @BeforeEach
    public void setUp() {
        System.setProperty("webdriver.chrome.driver", "C:\\Users\\andre\\Downloads\\chromedriver-win64\\chromedriver-win64\\chromedriver.exe");
        ChromeOptions options = new ChromeOptions();
        options.addArguments("--remote-allow-origins=*");
        driver = new ChromeDriver(options);
        driver.get("https://link.springer.com/");

        homePage = new HomePage(driver);
        homePage.acceptCookies();
        homePage.navigateToLogin();

        loginPage = new LoginPage(driver);
        loginPage.login("andreystasiuk1488@gmail.com", "qwertytop1228");
        homePage.performSearch("ahoj");
        homePage.navigateToOldVersion();
        homePage.openSettings();
        homePage.navigateToAdvancedSearch();

        AdvancedSearchPage advancedSearchPage = new AdvancedSearchPage(driver);
        advancedSearchPage.search("Page Object Model", "", "Selenium Testing", "", "", "", "2024", "2024");

        searchResultsPage = new SearchResultsPage(driver);
        searchResultsPage.filterByArticle();

        articles = new ArrayList<>(); // инициализация списка
    }

    @ParameterizedTest
    @CsvSource({
            "MUTTA: a novel tool for E2E web mutation testing, Published: 17 April 2023, https://doi.org/10.1007/s11219-023-09616-6",
            "Improving Android app exploratory testing with UI test cases using code change analysis, Published: 04 April 2024, https://doi.org/10.1007/s11334-024-00555-4",
            "SRI 2024: Scientific Program & Abstracts, Published: 19 March 2024, https://doi.org/10.1007/s43032-024-01501-2",
            "WALLETRADAR: towards automating the detection of vulnerabilities in browser-based cryptocurrency wallets, Published: 31 March 2024, https://doi.org/10.1007/s10515-024-00430-3"
    })
    public void testSearchSavedArticles(String articleTitle, String expectedPubDate, String expectedDOI) {
        int numberOfArticles = searchResultsPage.getNumberOfArticles();
        for (int i = 0; i < 4 && i < numberOfArticles; i++) {
            searchResultsPage.openArticle(i);

            articlePage = new ArticlePage(driver);
            String title = articlePage.getTitle();
            String doi = articlePage.getDOI();
            String pubDate = articlePage.getPubDate();

            articles.add(new Article(title, doi, pubDate));

            if (title.equals(articleTitle)) {
                assertThat(pubDate).isEqualTo(expectedPubDate);
                assertThat(doi).isEqualTo(expectedDOI);
            }

            driver.navigate().back();
        }
    }
}